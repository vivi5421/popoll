import inspect
from popoll.model.exception import Error, FatalError
import re
from . import DBModel
from datetime import datetime
from datetime import timedelta

DATETIME = '(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+)T(?P<hours>\d+):(?P<minutes>\d+):(?P<seconds>\d+).(?P<millis>\d+)Z'
YEAR = 'year'
MONTH = 'month'
DAY = 'day'
HOURS = 'hours'
MINUTES = 'minutes'
# SECONDS = 'seconds'
# MILLIS = 'millis'

class Date(DBModel):

    def __init__(self, db):
        super().__init__(db, 'dates')
        #json = day, time, description

    def _get(self, _):
        deadline_display = datetime.now() + timedelta(days=-7)

        def __sort(data):
            _date: re.Match[str] = re.match(DATETIME, data['day'])
            _time: re.Match[str] = re.match(DATETIME, data['time'])
            try:
                return int(_date.group(YEAR) + _date.group(MONTH) + _date.group(DAY) + _time.group(HOURS) + _time.group(MINUTES))
            except:
                return int(_date.group(YEAR) + _date.group(MONTH) + _date.group(DAY) + '0000')

        def _filter(data):
            try:
                _date: re.Match[str] = re.match(DATETIME, data['day'])
                date = datetime(int(_date.group(YEAR)), int(_date.group(MONTH)), int(_date.group(DAY)))
                return date >= deadline_display
            except Exception as e:
                print(f'Issue while trying to read date [{data}]')
                print(e)
                return False

        res = sorted(filter(_filter, self.table.all()), key=__sort)
        return res
