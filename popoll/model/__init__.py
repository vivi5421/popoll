from typing import Dict


class DBModel():

    def __init__(self, db, table):
        self.db = db
        self._table = table
        self.table = db['data'].table(table)

    def log(self, connexion, method, _id, json):
        self.db['history'].table('history').insert({
            'connexion': connexion,
            'db_table': self._table,
            'db_action': method,
            '_id': _id,
            'params': json
        })

    def get(self, json={}):
        return self._get(json)

    def _get(self, _):
        return self.table.all()

    def insert(self, connexion, json):
        _id = self._insert(json)
        json['_id'] = _id
        self.table.update(json, doc_ids=[_id])
        self.log(connexion, 'insert', _id, json)
        return _id

    def _insert(self, json):
        return self.table.insert(json)

    def update(self, connexion, _id, json):
        __id = self._update(_id=_id, json=json)
        self.log(connexion, 'update', _id, json)
        return __id


    def _update(self, _id, json):
        return self.table.update(json, doc_ids=[_id])

    def remove(self, connexion, _id):
        __id = self._remove(_id=_id)
        self.log(connexion, 'remove', _id, None)
        return __id

    def _remove(self, _id):
        self.table.remove(doc_ids=[_id])
        return _id