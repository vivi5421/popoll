import tinydb
import inspect

from popoll.model import DBModel
from popoll.model.exception import Error, FatalError

class Answer(DBModel):

    def __init__(self, db):
        super().__init__(db, 'answers')
        self._matching = None
        # json = date_id, user_id, value, timestamp

    def _get(self, json):
        if json.get('date_id', None) and json.get('user_id', None):
            q = tinydb.Query()
            return self.table.search((q.user_id == str(json['user_id'])) & (q.date_id == str(json['date_id'])))
        elif json.get('date_id', None):
            q = tinydb.Query()
            return self.table.search(q.date_id == str(json['date_id']))
        elif json.get('user_id', None):
            q = tinydb.Query()
            return self.table.search(q.user_id == str(json['user_id']))
        else:
            return self.table.all()

    def _insert(self, json):
        if self.already_exists(json):
            raise FatalError('Answer already registered for this Date and this User')
        return self.table.insert(json)

    def _update(self, _id, json):
        if not self.already_exists(json):
            raise FatalError('Missing previous data for this user and this date')
        if not self.get_max_timestamp(json) < json['timestamp']:
            raise Error('This message was sent before another one already considered, we can bypass')
        return self.table.update(json, doc_ids=[_id])
    
    def already_exists(self, json):
        q = tinydb.Query()
        return self.table.contains((q.date_id == json['date_id']) & (q.user_id == json['user_id']))

    def get_max_timestamp(self, json):
        q = tinydb.Query()
        return max([d['timestamp'] for d in self.table.search((q.date_id == json['date_id']) & (q.user_id == json['user_id']))])
