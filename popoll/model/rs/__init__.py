import flask
from popoll.model.answer import Answer

from popoll.model.date import Date
from popoll.model.user import User

def count_details_dates(dates, users, answers):
    output = {}
    for date in dates:
        output[date['_id']] = {}
        output[date['_id']]['yes'] = len([a for a in answers if a['date_id'] == str(date['_id']) and a['value'] == 'yes'])
        output[date['_id']]['no'] = len([a for a in answers if a['date_id'] == str(date['_id']) and a['value'] == 'no'])
        output[date['_id']]['unknown'] = len(users) - output[date['_id']]['yes'] - output[date['_id']]['no']
    return output

class PopollRS():

    def __init__(self, params, db, isWarning=False, isError=False, isFatal=False, message=None):
        self.params = params
        self.poll = params['poll']
        self.isWarning = isWarning
        self.isError = isError
        self.isFatal = isFatal
        self.message = message
        self.dates = Date(db).get()
        self.users = User(db).get()
        self.answers = Answer(db).get()
        self.total = {
            "users": {
                "count": len(self.users)
            },
            "dates": {
                "count": len(self.dates),
                "count_details": count_details_dates(self.dates, self.users, self.answers)
            }
        }



    def __str__(self):
        return str(vars(self))

    def __repr__(self):
        return self.__str__()

    def jsonify(self):
        return {
            "poll": self.poll,
            "content": {
                "dates": self.dates,
                "users": self.users,
                "answers": self.answers
            },
            "details": {
                "total": self.total
            },
            "isWarning": self.isWarning,
            "isError": self.isError,
            "isFatal": self.isFatal,
            "message": self.message
        }