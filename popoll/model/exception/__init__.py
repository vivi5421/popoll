class Error(BaseException):

    def __init__(self, message):
        super().__init__()
        self.message = message

    def getMessage(self):
        return self.message

class FatalError(BaseException):

    def __init__(self, message):
        super().__init__()
        self.message = message

    def getMessage(self):
        return self.message