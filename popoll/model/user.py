import tinydb
from popoll.model import DBModel
from popoll.model.exception import Error, FatalError


class User(DBModel):

    def __init__(self, db):
        super().__init__(db, 'users')
        # json = name

    def _insert(self, json):
        if self.already_exists(json):
            raise Error('User with this name already exists')
        return self.table.insert(json)

    def _update(self, _id, json):
        if self.already_exists(json):
            raise Error('User with this name already exists')
        return self.table.update(json, doc_ids=[_id])

    def already_exists(self, json):
        q = tinydb.Query()
        return len(self.table.search((q.name == json['name']))) > 0
