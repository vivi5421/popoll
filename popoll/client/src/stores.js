import { writable } from "svelte/store";

const storedSelectedUser = localStorage.getItem('selectedUser');
export const selectedUser = writable(storedSelectedUser ? storedSelectedUser : 'null');
const unsubscribe = selectedUser.subscribe((value) => {
    localStorage.setItem("selectedUser", value);
  });