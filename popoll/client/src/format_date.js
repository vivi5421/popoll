function formatDate(rawDate) {
    let opts = {
        weekday: "short",
        year: "2-digit",
        month: "short",
        day: "numeric",
    };
    return new Date(rawDate).toLocaleDateString("fr", opts);
}

function formatTime(rawDtime) {
    let opts = { hour: "2-digit", minute: "2-digit" };
    return new Date(rawDtime).toLocaleTimeString("fr", opts);
}

export const formatDT = (date) => {
    if (date?.time) {
        return formatDate(date.day) + ' (' + formatTime(date.time) + ')';
    }
    return formatDate(date?.day);
};