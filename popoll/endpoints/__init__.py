import flask
import json
import tinydb
import traceback

from popoll.model.answer import Answer
from popoll.model.date import Date
from popoll.model.exception import Error, FatalError
from popoll.model.rs import PopollRS
from popoll.model.user import User

class CRUD(object):

    @property
    def _default(self):
        raise NotImplementedError()

    def get(self, params):
        return self._default

    def post(self, params):
        return self._default

    def put(self, params):
        return self._default

    def delete(self, params):
        return self._default

    def process(self, method, params, verbose=True):
        if verbose: print(f'>>> ( in) {self.__class__.__name__} > {method} > {params}')
        self.db = {
            'data': tinydb.TinyDB(f'{params["poll"]}.db'),
            'history': tinydb.TinyDB(f'{params["poll"]}.history.db')
        }
        try:
            {
                'GET': self.get,
                'POST': self.post,
                'PUT': self.put,
                'DELETE': self.delete,
            }[method](params) # type: ignore
            response = PopollRS(params, self.db)
        except Error as e:
            response = PopollRS(params, self.db, isError=True, message= e.getMessage())
        except FatalError as e:
            response = PopollRS(params, self.db, isError=True, isFatal=True, message=e.getMessage())
        except Exception as e:
            print(f'!!! (err) ERROR\n{traceback.format_exc()}')
            response = PopollRS(params, self.db, isError=True, isFatal=True, message=traceback.format_exc())
        finally:
            response = flask.jsonify(response.jsonify())
            response.headers.add('Access-Control-Allow-Origin', '*')
            if verbose: print(f'<<< (out) {json.dumps(json.loads(response.data.decode("utf-8")))}')
            return response
