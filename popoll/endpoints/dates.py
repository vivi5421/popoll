from popoll.endpoints.polls import Polls
from popoll.model.answer import Answer
from popoll.model.date import Date
from . import CRUD

class Dates(CRUD):

    @classmethod
    def content(cls, params):
        return {
            'day': params['day'],
            'time': params.get('time', None),
            'description': params.get('description', None)
        }

    def post(self, params):
        return Date(self.db).insert(params['connexion'], Dates.content(params))

    def put(self, params):
        return Date(self.db).update(params['connexion'], params['_id'], Dates.content(params))

    def delete(self, params):
        _id = Date(self.db).remove(params['connexion'], params['_id'])
        for item in Answer(self.db).get({'date_id': params['_id']}):
            Answer(self.db).remove(params['connexion'], item['_id'])
        return _id