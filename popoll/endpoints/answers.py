from popoll.endpoints.polls import Polls
from popoll.model.answer import Answer
from . import CRUD

class Answers(CRUD):

    @classmethod
    def content(cls, params):
        return {
            'date_id': params['date_id'],
            'user_id': params['user_id'],
            'value': params['value'],
            'timestamp': params['timestamp']
        }

    def post(self, params):
        return Answer(self.db).insert(params['connexion'], Answers.content(params))

    def put(self, params):
        return Answer(self.db).update(params['connexion'], params['_id'], Answers.content(params))