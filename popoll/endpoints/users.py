from popoll.endpoints.polls import Polls
from popoll.model.answer import Answer
from popoll.model.user import User
from . import CRUD

class Users(CRUD):

    @classmethod
    def content(cls, params):
        return {
            'name': params['name']
        }

    def post(self, params):
        return User(self.db).insert(params['connexion'], Users.content(params))

    def put(self, params):
        return User(self.db).update(params['connexion'], params['_id'], Users.content(params))

    def delete(self, params):
        _id = User(self.db).remove(params['connexion'], params['_id'])
        for item in Answer(self.db).get({'user_id': params['_id']}):
            Answer(self.db).remove(params['connexion'], item['_id'])
        return _id


    