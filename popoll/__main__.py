#! /usr/bin/python3
import flask
import random
import json
import tinydb
from datetime import datetime

from flask_cors import CORS

from .endpoints.answers import Answers
from .endpoints.dates import Dates
from .endpoints.polls import Polls
#from .endpoints.root import Root
from .endpoints.users import Users


from .model.date import Date
from .model.user import User
from .model.answer import Answer

app = flask.Flask(__name__)
CORS(app)

# Path for our main Svelte page
@app.route("/")
def base():
    return flask.send_from_directory('client/public', 'index.html')

# Path for all the static files (compiled JS/CSS, etc.)
@app.route("/<path:path>")
def home(path):
    return flask.send_from_directory('client/public', path)

@app.route('/<string:poll>', methods=['GET'])
def p(poll):
    return flask.send_from_directory('client/public', 'index.html')

@app.route('/p/<string:poll>', methods=['GET'])
def polls(poll):
    return Polls().process(flask.request.method, get_params(flask.request, poll=poll))

@app.route('/p/<string:poll>/dates', methods=['POST', 'PUT', 'DELETE'])
def dates(poll):
    return Dates().process(flask.request.method, get_params(flask.request, poll=poll))

@app.route('/p/<string:poll>/users', methods=['POST', 'PUT', 'DELETE'])
def users(poll):
    return Users().process(flask.request.method, get_params(flask.request, poll=poll))

@app.route('/p/<string:poll>/<string:date_id>/<string:user_id>/answers', methods=['POST', 'PUT'])
def answers(poll, date_id, user_id):
    return Answers().process(flask.request.method, get_params(flask.request, poll=poll, date_id=date_id, user_id=user_id))

def get_params(request, poll=None, date_id=None, user_id=None):
    try:
        content = json.loads(request.get_data().decode('utf-8'))
    except:
        content = {}
    if poll: content['poll'] = poll
    if date_id: content['date_id'] = date_id
    if user_id: content['user_id'] = user_id
    content['connexion'] = {
        'ip': request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr),
        'browser': request.user_agent.browser,
        'version': request.user_agent.version and int(request.user_agent.version.split('.')[0]),
        'platform': request.user_agent.platform,
        'uas': request.user_agent.string,
        'timestamp': datetime.now().strftime('%d/%m/%Y %H:%M:%S')
    }
    return content

def run():
    app.run(debug=True, host='0.0.0.0', port=4444)

if __name__ == '__main__':
    run()