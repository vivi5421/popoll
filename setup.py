from setuptools import setup, find_packages
import unittest

with open("README.md", "r") as fh:
    long_description = fh.read()

def my_test_suite():
    test_loader = unittest.TestLoader()
    test_suite = test_loader.discover('tests', pattern='test_*.py')
    return test_suite

setup(
    name="popoll",
    packages=find_packages(exclude=['tests']),
    version="0.0.1",
    author="Nicolas Villard-Parriault",
    author_email="gitlab@villard.me",
    description="Poll website",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/vivi5421/pyquest",
    python_requires='>=3.6',
    include_package_data = True,
    package_data={
        "": ["*.yaml"],
        "": ["*.json"]
    },
    install_requires=[
        'flask',
        'flask_cors',
        'nanoid',
        'tinydb',
        'lazy-property'
    ]
)
